<?php
/**
 * Controller for error scenarios
 *
 * @author Adrian Ancona Novelo <soonick5@yahoo.com.mx>
 */
class ErrorController extends Zend_Controller_Action
{
    /**
     * Logs the error message and trace. Sends message and trace to the view
     * if in development environment
     */
    public function errorAction()
    {
        $this->_helper->layout->setLayout('error');

        $errors = $this->_getParam('error_handler');
        $exception = $errors->exception;
        $log = new Zend_Log(
            new Zend_Log_Writer_Stream(
                '/tmp/applicationException.log'
            )
        );
        $log->debug($exception->getMessage() . "\n" .
                $exception->getTraceAsString());

        // If we are in development environment we see the debug information
        if ('development' === APPLICATION_ENV) {
            $this->view->errorMessage = $exception->getMessage();
            $this->view->errorTrace = $exception->getTraceAsString();
        }
    }
}
