<?php
/**
 * Bootstrap
 * Initialize jarwebdev
 *
 * @author alejandro Morelos <alejandro.morelos@jarwebdev.com>
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * Bootstrap view. Initilize layout meta tags and favicon.
     */
    protected function _initViewResource()
    {
        $this->bootstrap('view');

        $this->view->doctype('HTML5');

        // Meta tags
        $this->view->headMeta()->setCharset('UTF-8');
        $this->view->headMeta()->appendHttpEquiv('Content-Type',
                'text/html; charset=UTF-8');

        // Favicon
        $this->view->headLink(
            array(
                'rel' => 'shortcut icon',
                'type' => 'image/png',
                'href' => '/images/favicon.png'
            )
        );
    }
}
